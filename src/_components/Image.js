import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Image extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        const {id, url} = this.props;
        return (
            <Link to={`/image/${id}`}>
                <img key={id} className="image" src={url} alt={id}/>
            </Link>
        );
    }
}

export default Image;
