import React, {Component} from 'react';
import Image from "./Image";

class ImageList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {images} = this.props;
        const list = images.map(item => <li><Image id={item.id} url={item.url}/></li>)


        return (
            <div>
                <ul>
                    {list}
                </ul>
            </div>
        );
    }
}

export default ImageList;
