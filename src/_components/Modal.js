import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import "./Modal.scss"
import {Images} from "../_action/Images";

class Modal extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(Images.getBigImage(parseInt(this.props.match.params.imageId, 10)))
    }

    render() {
        const {currentImage, closeHandler} = this.props;

        const comments = currentImage && currentImage.comments.map(item => <li>{item.text}</li>)
        return (
            <div className="modal-wrapper">
                {this.props.currentImage.loading &&
                <div className="modal-body">
                    ЗАГРУЗКА...
                </div>
                }

                {!this.props.currentImage.loading &&
                <div className="modal-body">
                    <Link to="/"><button onClick={closeHandler}>close</button></Link>
                    {currentImage && <img src={currentImage.url} alt="modal-image"/>}
                    <ul>
                        {comments}
                    </ul>
                </div>}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {currentImage} = state;
    return {
        currentImage
    }
}

export default connect(mapStateToProps)(Modal);
