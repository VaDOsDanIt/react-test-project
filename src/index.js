import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import {store} from "./_util/store";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Modal from "./_components/Modal";


ReactDOM.render(
    <Provider store={store}>

        <Router>
            <Switch>
                <Route path="/" component={App}/>
            </Switch>
        </Router>
    </Provider>

    ,
    document.getElementById('root')
);
