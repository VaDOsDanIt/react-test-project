import {GET_BIG_IMAGE, GET_IMAGES, REQUEST} from "../_const/ImagesConstants";

export const Images = {
    getAllImages,
    getBigImage
}

function getAllImages() {
    return dispatch => {
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        };

        fetch(`https://boiling-refuge-66454.herokuapp.com/images`, requestOptions)
            .then(data => data.json())
            .then(json => dispatch(success(json)));
    };

    function success(data) {
        return {type: GET_IMAGES, payload: {data}}
    }
}

function getBigImage(id) {
    return dispatch => {
        dispatch(request())
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        };

        fetch(`https://boiling-refuge-66454.herokuapp.com/images/${id}`, requestOptions)
            .then(data => data.json())
            .then(json => dispatch(success(json)));
    };

    function success(data) {
        return {type: GET_BIG_IMAGE, payload: {data}}
    }

    function request() {
        return {type: REQUEST}
    }
}
