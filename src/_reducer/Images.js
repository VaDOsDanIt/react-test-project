import {GET_IMAGES, GET_BIG_IMAGE, REQUEST} from "../_const/ImagesConstants";

const initialState = {
    img: [],
    currentImage: {
        image: "",
        comments: [],
        loading: false
    }
}

export function Images(state = initialState, action) {
    switch (action.type) {
        case GET_IMAGES:
            return {
                ...state,
                img: action.payload.data
            }
        case GET_BIG_IMAGE:
            return {
                ...state,
                currentImage: {
                    ...action.payload.data,
                    loading: false
                }
            }
        case REQUEST:
            return {
                ...state,
                currentImage: {
                    ...state.currentImage,
                    loading: true
                }
            }
        default:
            return state;
    }
}
