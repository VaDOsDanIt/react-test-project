import {createStore, applyMiddleware} from "redux";
import reduxThunk from 'redux-thunk';

import {Images} from '../_reducer/Images';

export const store = createStore(
    Images,
    applyMiddleware(
        reduxThunk
    )
)
