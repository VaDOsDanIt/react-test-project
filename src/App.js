import React, {Component} from 'react';
import {Images} from "./_action/Images"
import {connect} from "react-redux"
import PropTypes from 'prop-types';
import Modal from "./_components/Modal";
import ImageList from "./_components/ImageList";
import {Switch, Route, Router} from "react-router-dom"


class App extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.dispatch(Images.getAllImages())
    }

    render() {
        return (
            <div>
                <ImageList images={this.props.img} onClick={this.modalHandler}/>
                <Route exact={true} path="/image/:imageId" component={Modal}/>

            </div>
        );
    }
}

App.propTypes = {
    Images: {
        img: PropTypes.array
    }
}

const mapStateToProps = (state) => {
    const {img} = state;
    return {
        img
    }
}


export default connect(mapStateToProps)(App);
